import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';


class AuthenticationService {
  final FirebaseAuth _firebaseAuth;
  AuthenticationService(this._firebaseAuth);

  //Stream<User> get authStateChanges => _firebaseAuth.authStateChanges();

//1
  Future<dynamic> signIn({String email, String password}) async {
    try {
     var response=  await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
     sharedPreferences.setString('email', response.user.email);
      return response.user;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return "No user found for that email.";
      } else if (e.code == 'wrong-password') {
        return "Wrong password provided for that user.";
      } else {
        return "Something Went Wrong.";
      }
    }
  }

//2
  Future<dynamic> signUp({String email, String password}) async {
    print(email);
    print(password);
    try {
     var response= await _firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password,);
     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
     sharedPreferences.setString('email', response.user.email);
      return response.user;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return "The password provided is too weak.";
      } else if (e.code == 'email-already-in-use') {
        return "The account already exists for that email.";
      } else {
        return "Something Went Wrong.";
      }
    } catch (e) {
      print(e);
    }
  }
  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }
}
