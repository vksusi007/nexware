import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nexware/api_services.dart';
import 'package:nexware/countries.dart';
import 'package:nexware/repository.dart';

class CountryList extends StatefulWidget
{
  @override
  CountryListState createState() => CountryListState();
}

class CountryListState extends State<CountryList>
{
  List<Countries>  countries;
  @override
  void initState(){
    super.initState();
    callCountryList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        title: Text("Country List" , style: TextStyle(color: Colors.black),),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 2.0,
      ),
      body: countries == null?
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(child: CircularProgressIndicator()),
        ],
      ):
        ListView.builder(
            itemCount: countries.length,
            itemBuilder: (context, index)
            {
              return Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(color: Colors.grey)
                  )
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListTile(
                    leading: CircleAvatar(
                      child: Text(countries[index].name.substring(0,1)),
                      radius: 20,
                    ),
                    title: Text(countries[index].name),
                    subtitle: Text(countries[index].capital),
                  )
                ),
              );
            }),
    );
  }

  Future<void> callCountryList()
  async {
    var response = await ApiService().getCountryList();
    setState((){
     countries = response;
    });
  }
}