import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nexware/auth_service.dart';
import 'package:nexware/country_list.dart';
import 'package:nexware/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget
{
  String email;
  Home({this.email}):super();
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home>
{
  FirebaseAuth auth = FirebaseAuth.instance;
  @override
  void initState()
  {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Home",style: TextStyle(color: Colors.black),),
        centerTitle: true,
        elevation: 2.0,
        backgroundColor: Colors.white,
        actions: [
          IconButton(icon: Icon(Icons.logout, color: Colors.black,),
              onPressed: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.clear();
              AuthenticationService(auth).signOut();
              Navigator.pushReplacement(context, CupertinoPageRoute(builder: (context)=>
                LoginPage()));
              })
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: Column(
              children: [
                Text(widget.email, style: TextStyle(color: Colors.black, fontSize: 16.0),),
                SizedBox(height: 5.0,),
                RaisedButton(
                  onPressed: (){
                  Navigator.push(context, CupertinoPageRoute(builder: (context)=>
                   CountryList()));
                  },
                  child: Text("Get Countries", style: TextStyle(color: Colors.white),),
                 color: Colors.black,),
              ],
            ),
          ),
        ],
      ),
    );
  }
}