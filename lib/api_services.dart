import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:nexware/countries.dart';
class ApiService{

  getCountryList() async
  {
    var url = Uri.parse('https://restcountries.eu/rest/v2/all');
    var response = await http.get(url);
    print(response.statusCode);
   // print(response.body);
    List<dynamic> countryMap = jsonDecode(response.body);
    print(countryMap);
    List<Countries> countries = List<Countries>.from(countryMap.map((i)=>
       Countries.fromJson(i))).toList();
    return countries;
  }
}