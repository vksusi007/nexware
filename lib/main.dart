import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:nexware/home.dart';
import 'package:nexware/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String email = prefs.getString('email');
  runApp(MyApp(email));
}

class MyApp extends StatelessWidget {
  String mail;
  MyApp(this.mail):super();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: mail == null || mail==""?LoginPage():Home(email: mail,),
    );
  }
}

